
# Kafka Streaming Demo

To use, you need to have Kafka cluster deployed. But for the simplicity, we will use test class of Kafka Stream. It will allows us to implement functionality without running on environment but following the same logic. 

# Git clone repo Kafka Stream examples

Make sure you have git installed. Then you can clone:

```
git clone https://github.com/confluentinc/kafka-streams-examples.git
```

It is maven(mvn) based project open up with your favorite IDE.

## We will use integration test to implement our streaming logic.

[If you would like to learn more about what is integration test ?](https://en.wikipedia.org/wiki/Integration_testing)

**Open the source code of WordCountLambdaExample**

File is located in cloned git repo:
```
kafka-streams-examples/src/test/java/io/confluent/examples/stream/WordCountLambdaIntegrationTest
```

![Running Wordcount Example](img/runTests.png "Running Tests")

If you click green arrow it will execute test. Execute and verify, it is working.

Now, please take your time and have a look at source code and try to understand.


## Use integration-test code as a boiler plate and implement your KTable

* Your task is to implement count frequency of letters not including whitespace in Kafka Streams.

* Add your input to your code you can find in (test_file/count_letters.txt) by removing first line.

* To accomplish this task, you need to modify the following line:

![Running Letter Count Example](img/answerFrequency.png "Implement count freq of letters")

* You can also see your answer by adding following line to your code:

![Observe your output](img/printTest.png "Observe your result")

Note: once you change for observing your test can fail because you already read before adding observing line.
You need to do following change in order to get your test success again.
``` java
 assertThat(output.readKeyValuesToMap()).isEqualTo(expectedWordCounts); //changed from this
 assertThat(map).isEqualTo(expectedWordCounts); // to this
```
* You can use the answer text file to compare your answer.


## Stream to Stream Joins

File is located in cloned git repo:
```
kafka-streams-examples/src/test/java/io/confluent/examples/stream/StreamToStreamJoinIntegrationTest
```

## Stream to Table Joins

File is located in cloned git repo:
```
kafka-streams-examples/src/test/java/io/confluent/examples/stream/StreamToTableJoinIntegrationTest
```
